import { configureStore, combineReducers } from '@reduxjs/toolkit';
import { useSelector as nativeUseSelector} from 'react-redux'

const rootReducer = combineReducers({
})

export const store = configureStore({
  reducer: rootReducer
})

export const useSelector = nativeUseSelector;