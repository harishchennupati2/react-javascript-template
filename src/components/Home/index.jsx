import React from 'react';
import _ from 'lodash';

import FetchTreasures from 'services/fetchTreasures';
import { Link } from 'react-router-dom';
import './styles.scss';

const Home = () => {
  const [, setLevel1Data] = React.useState([]);
  const [, setMergeData] = React.useState([]);

  const generateRandomSets = (a, b, c) => {
    let randomSets = [];
    let count = 0;
    let obj;
    while (true) {
      obj = {
        treasure: _.sample(a),
        latlong: _.sample(b),
        admin: _.sample(c)
      };
      let compareCount = randomSets.filter(
        // eslint-disable-next-line
        set =>
          // eslint-disable-next-line
          set.treasure === obj.treasure || set.latlong[0] === obj.latlong[0] || set.latlong[1] === obj.latlong[1] || set.admin === obj.admin
      ).length;
      if (compareCount === 0) {
        randomSets = [...randomSets, obj];
        count += 1;
      }
      if (count === 9) break;
    }
    return randomSets;
  };

  const fetchData = async () => {
    const response = await FetchTreasures.fetchTreasures();
    setLevel1Data(response);
    localStorage.setItem('treasureData', JSON.stringify(response));
    const mergeFinalData = generateRandomSets(response.treasures, _.chunk(response.latlong, 2), response.admin);
    localStorage.setItem('treasureDataRandom', JSON.stringify(mergeFinalData));
    setMergeData(mergeFinalData);
  };

  const level1Data = JSON.parse(localStorage.getItem('treasureData')) || [];
  const mergeData = JSON.parse(localStorage.getItem('treasureDataRandom')) || [];
  return (
    <div className='homeContainer'>
      <button className='button is-primary container' onClick={() => fetchData()}>
        Retrieve Remote Data
      </button>
      <div className='container'>
        <div>
          <h1 className='title'>Treasure</h1>
          {level1Data?.treasures?.map((val, index) => {
            return <li key={index + 1}>{val}</li>;
          })}
        </div>
        <div>
          <h1 className='title'>LatLong</h1>
          {_.chunk(level1Data?.latlong, 2)?.map((val, index) => {
            return (
              <li key={index + 1}>
                {val[0]} {val[1]}
              </li>
            );
          })}
        </div>
        <div>
          <h1 className='title'>Admin</h1>
          {level1Data?.admin?.map((val, index) => {
            return <li key={index + 1}>{val}</li>;
          })}
        </div>
      </div>
      <div className='mergeContainer'>
        {!_.isEmpty(mergeData) && <h2 className='title is-4'>TCO 20 Treasures </h2>}
        {/* <p> {!_.isEmpty(level1Data) && !generating && 'Generating Random sets Please Wait ...'}</p> */}
      </div>
      <div className='container'>
        {!_.isEmpty(mergeData) && (
          <table className='table is-bordered is-fullwidth'>
            <thead>
              <tr>
                <th>Treasure</th>
                <th>Lat & Long</th>
                <th>Admin</th>
                <th>Location</th>
              </tr>
            </thead>
            <tbody>
              {_.map(mergeData, node => {
                return (
                  <tr>
                    <td>{node.treasure}</td>
                    <td>
                      {node.latlong[0]}
                      {'      '}
                      {node.latlong[1]}
                    </td>
                    <td>{node.admin}</td>
                    <td>
                      <Link
                        to={{
                          pathname: '/treasure',
                          state: node
                        }}
                      >
                        <button className='button is-link'>View Location</button>
                      </Link>
                    </td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        )}
      </div>
    </div>
  );
};

Home.propTypes = {};

export default Home;
