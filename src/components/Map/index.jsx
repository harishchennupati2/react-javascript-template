import React from 'react';
import { useLocation } from 'react-router-dom';
import './styles.scss';

import mapboxgl from 'mapbox-gl';
import 'mapbox-gl/dist/mapbox-gl.css';
mapboxgl.accessToken = 'pk.eyJ1IjoiaGFyaXNoY2giLCJhIjoiY2toajJ6NmgxMDIwMjJybm9rMjlnZm95aCJ9.1K5y-JgQU-DUEUuYKqViJQ';

const Map = () => {
  let map, refContainer;
  const location = useLocation();
  const { latlong, treasure } = location.state;
  React.useEffect(() => {
    try {
      // eslint-disable-next-line
      map = new mapboxgl.Map({
        container: refContainer,
        style: 'mapbox://styles/mapbox/streets-v11',
        center: [latlong[1], latlong[0]],
        zoom: 12.5
      });
       new mapboxgl.Popup({ closeOnClick: false })
        .setLngLat([latlong[1], latlong[0]])
        .setHTML(`<h1>${treasure}</h1>`)
        .addTo(map);
    } catch (error) {
      alert(error.message);
    }
    // Clean up on unmount
    // return () => map.remove();
  }, []);

  return (
    <div className='mapContainer1'>
      <div ref={el => (refContainer = el)} className='mapContainer' />
    </div>
  );
};

Map.propTypes = {};

export default Map;
