import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';

import Home from 'components/Home';
import Map from 'components/Map';

export const Routing = () => (
  <Router>
    <Switch>
      <Route path='/' exact={true} component={Home} />
      <Route path='/treasure' exact={true} component={Map} />
    </Switch>
  </Router>
);
