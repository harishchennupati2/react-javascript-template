import React from 'react';
import { Provider } from 'react-redux';
import { Routing } from './Routing'
import { store } from './store';

function App() {
  return (
    <Provider store={store}>
      <Routing />
      {/* Loading Service if Required */}
    </Provider>
  );
}

export default App;
