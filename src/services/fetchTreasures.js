import axios, { handleError } from '.';

const parseData = (data) => {
  console.log(data);
  const leftBracket = data.indexOf("[");
  if (leftBracket !== -1) {
    data = data.slice(leftBracket, data.length - 1);
  }
  data = data.replace('[', '')
  data = data.replace(']', '')
  data = data.replace(';', '')
  data = data.replaceAll('"', '');
  return data.split(',').map((val) => val.trim());
}


class FetchTreasures {
  static async fetchTreasures() {
    try {
      const treasures = await axios.get('/6WbTKhyUOfMFk7DxD87Kfw/56b4a43d94915f4db542e2bff969f4f7/treasure.js')
      const latlong = await axios.get('/701h6IJoqhYp41jLifg6Z1/4d60462b8cf0caaa60c01f6ff2cf034d/latlong.js')
      const admin = await axios.get('/3i3XqdM2tnC2QFR7pIQOrw/ebd07d2a06aac01e2396158b72bc1897/admin.js')
      return {
        treasures: parseData(treasures.data),
        latlong: parseData(latlong.data),
        admin: parseData(admin.data)
      }
    }
    catch (error) {
      handleError(error)
    }
  }

}

export default FetchTreasures;