import axios from 'axios';
import _ from 'lodash';

const axiosInstance = axios.create({
  baseURL: process.env.REACT_APP_BACKEND_URL
})

/**
 * Handle network error
 * @param e error object
 */
export const handleError = (e) => {
  let errorMessage = '';
  errorMessage = _.get(e, 'response.data.message');
  if (!errorMessage) {
    errorMessage = e.message;
  }
  if (!errorMessage) {
    errorMessage = 'An error has been encountered, please try again!';
  }
  if (errorMessage && typeof errorMessage === 'object') {
    errorMessage = JSON.stringify(errorMessage);
  }
  return { errorMessage };
};

export default axiosInstance;


// Create Service class and add methods